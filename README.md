# Product Sync

This package retrieve updated information about products given an URL

Supported ecommerces:
- A Brand Br
- Água Doce
- Amaro
- Animale
- BoBo
- Cantão
- Cia Marítima
- Farm Rio (Pendente)
- Flaminga
- Fyi Store
- Le Lis Blanc
- Lezalez
- Lojas Renner
- Luiza Barcelos (Pendente)
- Morena Rosa
- Oh Boy
- Rosa Chá
- Sacada
- Shop2Gether
- Style Market
- Zinzane


### Usage
```js
const productsync = require('@oipati/products-sync');

const product = await productsync({
  ecommerce: 'shop2gether',
  url: 'https://ecommerce.url',
});
```

Are two possible outputs, the product doesn't exists anymore or the updated data about the product.

#### Product doesn't exist
```js
{
  "exists": false
}
```

#### Product updated
```js
{
  "exists": Boolean,
  "hasStock": Boolean,
  "oldPrice": Number,
  "price": Number,
  "description": String,
}
```


#### DEBUG

export the `DEBUG_SYNCER` env var

```sh
export DEBUG_SYNCER=true
node index.js
```
