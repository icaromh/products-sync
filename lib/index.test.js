const assert = require('assert');
const productssync = require('./index');

describe('ProductsSync', () => {

  describe('#productsync', () => {
    it('should throw an error if Ecommerce not implemented', async () => {
      try {
        const p = await productssync({
          'ecommerce': 'dummy',
          'url': 'http://dummy.com',
        })
      } catch (err) {
        assert.equal(err.message, 'Ecommerce not implemented: dummy')
      }
    });
  });
});
