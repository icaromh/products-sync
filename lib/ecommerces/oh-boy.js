const Ecommerce = require('../ecommerce')

/**
 * OhBoy Data Crawler
 * Method: requests (curl like)
 */
class OhBoy extends Ecommerce {

  constructor(url) {
    super(url)
  }

  async startup(){
    await this.getDocument();
  }

  async _getPrice(selector) {
    const price = this.document(selector)
    return this._normalizeValue(price.text())
  }

  async getOldPrice() {
    const selector = '.x-info-product .skuListPrice';
    return this._getPrice(selector);
  }

  async getPrice() {
    let selector = '.x-info-product .skuBestPrice';
    const specialPrice = await this._getPrice(selector);

    if(specialPrice) {
      return specialPrice;
    }

    selector = '.x-info-product .skuPrice';
    return this._getPrice(selector);
  }

  async getHasStock() {
    const selector = '.x-info-product .skuPrice';
    return this.document && this.document(selector).length !== 0;
  }

  async getDescription() {
    const selector = '.x-info-product .productDescription';
    const description = this.document(selector).html();
    return description.replace(/(\t|\n)/ig, '').trim();
  }

  async productExists() {
    const selector = '.x-info-product';
    return this.document && this.document(selector).length !== 0;
  }
}

module.exports = OhBoy;
