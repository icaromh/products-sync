const Ecommerce = require('../ecommerce')

/**
 * Example Data Crawler
 */
class Example extends Ecommerce {

  /**
   * Required
   * Constructor
   * @param {String} url the URL of the product
   */
  constructor(url) {
    super(url)
  }

  /**
   * Required Method
   * executed before the `run` method
   * in Browser Crawler: should start the page
   * in Curl Crawler: should make the request
   */
  async startup(){
    // code
  }

  /**
   * Required in Browser Crawler
   * executed after the `run` method
   * In Browser Crawler: should closes the browser
   * In Curl Crawler: should do nothing
   */
  async teardown(){
    // code
  }

  /**
   * Required Method
   * The first method executed
   * should verify if the product really exists
   * @returns {boolean} The product exists?
   */
  async productExists() {
    // code
  }

  /**
   * Required Method
   * Should get the actual product price
   * @returns {Number} The actual product's price
   */
  async getPrice() {
    // code
  }

  /**
   * Required Method
   * Should verify if has an old price
   * (if it has should be higher than the actual price)
   *
   * @returns {Number} the old product's price
   * @returns {boolean} False if doesn't have any old price
   */
  async getOldPrice() {
    // code
  }

  /**
   * Required Method
   * Should verify if the product it is out of stock or not
   * @returns {boolean} has stock?
   */
  async getHasStock() {
    // code
  }

  /**
   * Required Method
   * Should return the updated description
   * @returns {String} the description
   */
  async getDescription() {
    // code
  }

}


module.exports = Example;
