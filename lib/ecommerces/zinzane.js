const Ecommerce = require('../ecommerce')

/**
 * Zinzane Data Crawler
 * Method: requests (curl like)
 */
class Zinzane extends Ecommerce {

  constructor(url) {
    super(url)
  }

  async startup(){
    await this.getDocument();
  }

  async _getPrice(selector) {
    const price = this.document(selector)
    return this._normalizeValue(price.text())
  }

  async getOldPrice() {
    const selector = '.product-info .skuListPrice';
    return this._getPrice(selector);
  }

  async getPrice() {
    let selector = '.product-info .skuBestPrice';
    const specialPrice = await this._getPrice(selector);

    if(specialPrice) {
      return specialPrice;
    }

    selector = '.product-info .skuPrice';
    return this._getPrice(selector);
  }

  async getHasStock() {
    const selector = '.product-info .skuPrice';
    return this.document && this.document(selector).length !== 0;
  }

  async getDescription() {
    const selector    = '.product-info .productDescription';
    const description = this.document(selector).html();
    if(description == '' || description == null){
      return description;
    }
    return description.replace(/(\t|\n)/ig, '').trim();
  }

  async productExists() {
    const selector = '.product-info';
    return this.document && this.document(selector).length !== 0;
  }
}

module.exports = Zinzane;
