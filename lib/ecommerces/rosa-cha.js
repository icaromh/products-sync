const Ecommerce = require('../ecommerce')

/**
 * RosaCha Data Crawler
 * Method: requests (curl like)
 */
class RosaCha extends Ecommerce {

  constructor(url) {
    super(url)
  }

  async startup(){
    await this.getDocument();
  }

  async _getPrice(selector) {
    const price = this.document(selector)
    return this._normalizeValue(price.text())
  }

  async getOldPrice() {
    const selector = '.x-product-details-group .skuListPrice';
    return this._getPrice(selector);
  }

  async getPrice() {
    let selector = '.x-product-details-group .skuBestPrice';
    const specialPrice = await this._getPrice(selector);

    if(specialPrice) {
      return specialPrice;
    }

    selector = '.x-product-details-group .skuPrice';
    return this._getPrice(selector);
  }

  async getHasStock() {
    const selector = '.x-product-details-group .skuPrice';
    return this.document && this.document(selector).length !== 0;
  }

  async getDescription() {
    const selector = '.x-product-details-group .productDescription';
    const description = this.document(selector).html();
    return description.replace(/(\t|\n)/ig, '').trim();
  }

  async productExists() {
    const selector = '.x-product-details-group';
    return this.document && this.document(selector).length !== 0;
  }
}

module.exports = RosaCha;
