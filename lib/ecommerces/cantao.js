const Ecommerce = require('../ecommerce')

/**
 * Cantao Data Crawler
 * Method: requests (curl like)
 */
class Cantao extends Ecommerce {

  constructor(url) {
    super(url)
  }

  async startup(){
    await this.getDocument();
  }

  async _getPrice(selector) {
    const price = this.document(selector)
    return this._normalizeValue(price.text())
  }

  async getOldPrice() {
    const selector = '.mainProductInfo .skuListPrice';
    return this._getPrice(selector);
  }

  async getPrice() {
    let selector = '.mainProductInfo .skuBestPrice';
    const specialPrice = await this._getPrice(selector);

    if(specialPrice) {
      return specialPrice;
    }

    selector = '.mainProductInfo .skuPrice';
    return this._getPrice(selector);
  }

  async getHasStock() {
    const selector = '.mainProductInfo .skuPrice';
    return this.document && this.document(selector).length !== 0;
  }

  async getDescription() {
    const selector = '.mainProductInfo .productDescription';
    const description = this.document(selector).html();
    return description.replace(/(\t|\n)/ig, '').trim();
  }

  async productExists() {
    const selector = '.mainProductInfo';
    return this.document && this.document(selector).length !== 0;
  }
}

module.exports = Cantao;
