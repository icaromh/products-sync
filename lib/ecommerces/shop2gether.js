const Ecommerce = require('../ecommerce')

/**
 * Shop2Gether Data Crawler
 * Method: requests (curl like)
 */
class Shop2gether extends Ecommerce {

  constructor(url) {
    super(url)
  }

  async startup(){
    await this.getDocument();
  }

  async _getPrice(selector) {
    const price = this.document(selector)
    return this._normalizeValue(price.text())
  }

  async getOldPrice() {
    const selector = '.product-essential .old-price > .price';
    return this._getPrice(selector);
  }

  async getPrice() {
    let selector = '.product-essential .special-price > .price';
    const specialPrice = await this._getPrice(selector);

    if(specialPrice) {
      return specialPrice;
    }

    selector = '.product-essential .regular-price > .price';
    return this._getPrice(selector);
  }

  async getHasStock() {
    const selector = '.availability .value';
    return this.document(selector).text() === 'Em estoque';
  }

  // @TODO: scrap the sizes of this product
  getSizes() {
    // get the <script> tag and search for the StockStatus
    function getStockInfo() {
      const selector = '#product-options-wrapper script';
      const script = this.document(selector).html();
      const scriptData = new RegExp(/new StockStatus\((.*)\)/ig).exec(script)
      let data = {};
      if (scriptData[1]) {
        data = JSON.parse(scriptData[1]);
      }

      return data;
    }

    // get the <script> tag and search for sizes info
    const getSizesInfo = () => {
      const selector = 'body';
      const script = this.document(selector).html();
      const scriptData = new RegExp(/new Product.Config\((.*)\)/ig).exec(script)
      let data = {};
      if (scriptData[1]) {
        data = JSON.parse(scriptData[1]);
      }
      return data;
    }

    console.log(getSizesInfo());
    return '';
  }

  async getDescription() {
    const selector = '.new-product-tabs-desc-content';
    const description = this.document(selector).html();
    return description.replace(/(\t|\n)/ig, '').trim();
  }

  async productExists() {
    const selector = '.availability .value';
    return this.document && this.document(selector).length !== 0;
  }
}


module.exports = Shop2gether;
