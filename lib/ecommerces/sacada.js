const Ecommerce = require('../ecommerce')

/**
 * Sacada Data Crawler
 * Method: requests (curl like)
 */
class Sacada extends Ecommerce {

  constructor(url) {
    super(url)
  }

  async startup(){
    await this.getDocument();
  }

  async _getPrice(selector) {
    const price = this.document(selector)
    return this._normalizeValue(price.text())
  }

  async getOldPrice() {
    const selector = '.x-product-infos .skuListPrice';
    return this._getPrice(selector);
  }

  async getPrice() {
    let selector = '.x-product-infos .skuBestPrice';
    const specialPrice = await this._getPrice(selector);

    if(specialPrice) {
      return specialPrice;
    }

    selector = '.x-product-infos .skuPrice';
    return this._getPrice(selector);
  }

  async getHasStock() {
    const selector = '.x-product-infos .skuPrice';
    return this.document && this.document(selector).length !== 0;
  }

  async getDescription() {
    const selector = '.x-product-infos .productName';
    const description = this.document(selector).html();
    return description.replace(/(\t|\n)/ig, '').trim();
  }

  async productExists() {
    const selector = '.x-product-infos';
    return this.document && this.document(selector).length !== 0;
  }
}

module.exports = Sacada;
