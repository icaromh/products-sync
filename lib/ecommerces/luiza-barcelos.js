const Ecommerce = require('../ecommerce')

/**
 * LuizaBarcelos Data Crawler
 * Method: requests (curl like)
 */
class LuizaBarcelos extends Ecommerce {

  constructor(url) {
    super(url)
  }

  async startup(){
    await this.getDocument();
  }

  async _getPrice(selector) {
    const price = this.document(selector)
    return this._normalizeValue(price.text())
  }

  async getOldPrice() {
    const selector = '.price-from';
    return await this._getPrice(selector);
  }

  async getPrice() {
    let selector = '.price-to';
    const specialPrice = await this._getPrice(selector);

    if(specialPrice) {
      return specialPrice;
    }

    selector = '.price-from';
    return this._getPrice(selector);
  }

  async getHasStock() {
    const selector = '.skuPrice';
    return this.document && this.document(selector).length !== 0;
  }

  async getDescription() {
    const selector = '#descriptionSection';
    const description = this.document(selector).html();
    return description.replace(/(\t|\n)/ig, '').trim();
  }

  async productExists() {
    const selector = '#info-product';
    return this.document && this.document(selector).length !== 0;
  }
}

module.exports = LuizaBarcelos;
