const Ecommerce = require('../ecommerce')

/**
 * StyleMarket Data Crawler
 * Method: requests (curl like)
 */
class StyleMarket extends Ecommerce {

  constructor(url) {
    super(url)
  }

  async startup(){
    await this.getDocument();
  }

  async _getPrice(selector) {
    const price = this.document(selector)
    return this._normalizeValue(price.text())
  }

  async getOldPrice() {
    const selector = '.detalhesProduto .total';
    return this._getPrice(selector);
  }

  async getPrice() {
    let selector = '.detalhesProduto .total';
    const specialPrice = await this._getPrice(selector);

    if(specialPrice) {
      return specialPrice;
    }

    selector = '.detalhesProduto .total';
    return this._getPrice(selector);
  }

  async getHasStock() {
    const selector = '.detalhesProduto .total';
    return this.document && this.document(selector).length !== 0;
  }

  async getDescription() {
    const selector = '.detalhesProduto .texto';
    const description = this.document(selector).html();
    return description.replace(/(\t|\n)/ig, '').trim();
  }

  async productExists() {
    const selector = '.detalhesProduto';
    return this.document && this.document(selector).length !== 0;
  }
}

module.exports = StyleMarket;
