const Ecommerce = require('../ecommerce')

/**
 * BoBo Data Crawler
 * Method: requests (curl like)
 */
class BoBo extends Ecommerce {

  constructor(url) {
    super(url)
  }

  async startup(){
    await this.getDocument();
  }

  async _getPrice(selector) {
    const price = this.document(selector)
    return this._normalizeValue(price.text())
  }

  async getOldPrice() {
    const selector = '.product-main__content-info .skuListPrice';
    return this._getPrice(selector);
  }

  async getPrice() {
    let selector = '.product-main__content-info .skuBestPrice';
    const specialPrice = await this._getPrice(selector);

    if(specialPrice) {
      return specialPrice;
    }

    selector = '.product-main__content-info .skuPrice';
    return this._getPrice(selector);
  }

  async getHasStock() {
    const selector = '.product-main__content-info .skuPrice';
    return this.document && this.document(selector).length !== 0;
  }

  async getDescription() {
    const selector = '.product-main__content-info .productDescription';
    const description = this.document(selector).html();
    return description.replace(/(\t|\n)/ig, '').trim();
  }

  async productExists() {
    const selector = '.product-main__content-info';
    return this.document && this.document(selector).length !== 0;
  }
}

module.exports = BoBo;
