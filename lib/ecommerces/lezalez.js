const Ecommerce = require('../ecommerce')

/**
 * Lezalez Data Crawler
 * Method: requests (curl like)
 */
class Lezalez extends Ecommerce {

  constructor(url) {
    super(url)
  }

  async startup(){
    await this.getDocument();
  }

  async _getPrice(selector) {
    const price = this.document(selector)
    return this._normalizeValue(price.text())
  }

  async getOldPrice() {
    const selector = '.information-products .title-black';
    return this._getPrice(selector);
  }

  async getPrice() {
    let selector = '.information-products .title-black';
    const specialPrice = await this._getPrice(selector);

    if(specialPrice) {
      return specialPrice;
    }

    selector = '.information-products .title-black';
    return this._getPrice(selector);
  }

  async getHasStock() {
    const selector = '.information-products .title-black';
    return this.document && this.document(selector).length !== 0;
  }

  async getDescription() {
    const selector = '.information-products .hide-for-small-only';
    const description = this.document(selector).html();
    return description.replace(/(\t|\n)/ig, '').trim();
  }

  async productExists() {
    const selector = '.information-products';
    return this.document && this.document(selector).length !== 0;
  }
}

module.exports = Lezalez;
