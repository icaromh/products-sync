const Ecommerce = require('../ecommerce')

/**
 * FarmRio Data Crawler
 * Method: puppeteer (chrome)
 */
class FarmRio extends Ecommerce {

  constructor(url) {
    super(url)
  }

  async startup() {
    await this.launchBrowser();
  }

  async teardown() {
    await this.closeBrowser();
  }

  async _getPrice(selector) {
    const price = await this.page.evaluate((selector) => {
      const el = document.querySelector(selector);
      if (!el) {
        return false;
      }
      return el.innerText;
    }, selector);

    return this._normalizeValue(price)
  }

  async getOldPrice() {
    const selector = '.product__info .skuListPrice';
    const price = await this._getPrice(selector);

    if (!price) return false;
    return price;
  }

  async getPrice() {
    let selector = '.product__info .skuBestPrice';
    let price = await this._getPrice(selector);

    if (!price) {
      selector = '.product__info .skuPrice';
      price = await this._getPrice(selector);
    }

    return price;
  }

  async getHasStock() {
    const selector = '.product__info';
    const hasStock = await this.page.evaluate((selector) => {
      const el = document.querySelector(selector);
      return !!(el && el.length !== 0);
    }, selector);

    return hasStock;
  }

  async getDescription() {
    const selector = '.product__info .productName';
    const description = await this.page.evaluate((selector) => {
      const el = document.querySelector(selector);
      if (!el) {
        return false;
      }
      return el.innerText;
    }, selector);

    return description;
  }

  async productExists() {
    const selector = '.product__info';
    const exists = await this.page.evaluate((selector) => {
      const el = document.querySelector(selector);
      return !!(el && el.length !== 0);
    }, selector);

    return exists;
  }

}

module.exports = FarmRio;
