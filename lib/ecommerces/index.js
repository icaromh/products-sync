const ABrandBr      = require('./a-brand-br');
const AguaDoce      = require('./agua-doce');
const Amaro         = require('./amaro');
const Animale       = require('./animale');
const BoBo          = require('./bo-bo');
const Cantao        = require('./cantao');
const CiaMaritima   = require('./cia-maritima');
const FarmRio       = require('./farm-rio');
const Flaminga      = require('./flaminga');
const Fyistore      = require('./fyistore');
const LeLisBlanc    = require('./le-lis-blanc');
const Lezalez       = require('./lezalez');
const LojasRenner   = require('./lojas-renner');
const LuizaBarcelos = require('./luiza-barcelos');
const MorenaRosa    = require('./morena-rosa');
const OhBoy         = require('./oh-boy');
const RosaCha       = require('./rosa-cha');
const Sacada        = require('./sacada');
const Shop2gether   = require('./shop2gether');
const StyleMarket   = require('./style-market');
const Zinzane       = require('./zinzane');

module.exports = {
    'a-brand-br'    : ABrandBr,
    'agua-doce'     : AguaDoce,
    amaro           : Amaro,
    animale         : Animale,
    'bo-bo'         : BoBo,
    cantao          : Cantao,
    'cia-maritima'  : CiaMaritima,
    // 'farm-rio'      : FarmRio,
    'flaminga'      : Flaminga,
    fyistore        : Fyistore,
    'le-lis-blanc'  : LeLisBlanc,
    'lezalez'       : Lezalez,
    'lojas-renner'  : LojasRenner,
    // 'luiza-barcelos': LuizaBarcelos,
    'morena-rosa'   : MorenaRosa,
    'oh-boy'        : OhBoy,
    'rosa-cha'      : RosaCha,
    sacada          : Sacada,
    shop2gether     : Shop2gether,
    'style-market'  : StyleMarket,
    zinzane         : Zinzane,
}