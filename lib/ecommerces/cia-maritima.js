const Ecommerce = require('../ecommerce')

/**
 * CiaMaritima Data Crawler
 * Method: requests (curl like)
 */
class CiaMaritima extends Ecommerce {

  constructor(url) {
    super(url)
  }

  async startup(){
    await this.getDocument();
  }

  async _getPrice(selector) {
    const price = this.document(selector)
    return this._normalizeValue(price.text())
  }

  async getOldPrice() {
    const selector = '.skuListPrice';
    return this._getPrice(selector);
  }

  async getPrice() {
    let selector = '.skuBestPrice';
    const specialPrice = await this._getPrice(selector);

    if(specialPrice) {
      return specialPrice;
    }

    selector = '.skuPrice';
    return this._getPrice(selector);
  }

  async getHasStock() {
    const selector = '.skuPrice';
    return this.document && this.document(selector).length !== 0;
  }

  async getDescription() {
    const selector = '.productDescription';
    const description = this.document(selector).html();
    return description.replace(/(\t|\n)/ig, '').trim();
  }

  async productExists() {
    const selector = '#product-details';
    return this.document && this.document(selector).length !== 0;
  }
}

module.exports = CiaMaritima;
