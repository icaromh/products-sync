const Ecommerce = require('../ecommerce')

/**
 * Flaminga Data Crawler
 * Method: requests (curl like)
 */
class Flaminga extends Ecommerce {

  constructor(url) {
    super(url)
  }

  async startup(){
    await this.getDocument();
  }

  async _getPrice(selector) {
    const price = this.document(selector)
    return this._normalizeValue(price.text())
  }

  async getOldPrice() {
    const selector = '.infosProduto .skuListPrice';
    return this._getPrice(selector);
  }

  async getPrice() {
    let selector = '.infosProduto .skuBestPrice';
    const specialPrice = await this._getPrice(selector);

    if(specialPrice) {
      return specialPrice;
    }

    selector = '.infosProduto .skuPrice';
    return this._getPrice(selector);
  }

  async getHasStock() {
    const selector = '.infosProduto .skuPrice';
    return this.document && this.document(selector).length !== 0;
  }

  async getDescription() {
    const selector = '.infosProduto .productDescription';
    const description = this.document(selector).html();
    return description.replace(/(\t|\n)/ig, '').trim();
  }

  async productExists() {
    const selector = '.infosProduto';
    return this.document && this.document(selector).length !== 0;
  }
}

module.exports = Flaminga;
