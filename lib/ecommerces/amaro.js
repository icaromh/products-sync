const Ecommerce = require('../ecommerce')

/**
 * Amaro Data Crawler
 * Method: puppeteer (chrome)
 */
class Amaro extends Ecommerce {

  constructor(url) {
    super(url)
  }

  async startup() {
    await this.launchBrowser();
  }

  async teardown() {
    await this.closeBrowser();
  }

  async _getPrice(selector) {
    const price = await this.page.evaluate((selector) => {
      const el = document.querySelector(selector);
      if (!el) {
        return false;
      }
      return el.innerText;
    }, selector);

    return this._normalizeValue(price)
  }

  async getOldPrice() {
    const selector = '.app__product__regular-price--sale';
    const price = await this._getPrice(selector);

    if (!price) return false;
    return price;
  }

  async getPrice() {
    let selector = '.app__product__actual-price';
    let price = await this._getPrice(selector);

    if (!price) {
      selector = '.app__product__regular-price';
      price = await this._getPrice(selector);
    }

    return price;
  }

  async getHasStock() {
    const selector = '.app__product__restock .app__product__restock__header'
    const hasStock = await this.page.evaluate((selector) => {
      const el = document.querySelector(selector);
      if (el && el.innerText === 'Infelizmente não temos esse produto em estoque.') {
        return false;
      }
      return true;
    }, selector);

    return hasStock;
  }

  async getDescription() {
    const selector = '.app__product__details';
    const description = await this.page.evaluate((selector) => {
      const el = document.querySelector(selector);
      if (!el) {
        return false;
      }
      return el.innerText;
    }, selector);

    return description;
  }

  async productExists() {
    const selector = '.app__product';
    const exists = await this.page.evaluate((selector) => {
      const el = document.querySelector(selector);
      return !!(el && el.length !== 0);
    }, selector);

    return exists;
  }

}

module.exports = Amaro;
