const Ecommerce = require('../ecommerce')

/**
 * MorenaRosa Data Crawler
 * Method: requests (curl like)
 */
class MorenaRosa extends Ecommerce {

  constructor(url) {
    super(url)
  }

  async startup(){
    await this.getDocument();
  }

  async _getPrice(selector) {
    const price = this.document(selector)
    return this._normalizeValue(price.text())
  }

  async getOldPrice() {
    const selector = '.bf-product__wrapper .skuListPrice';
    return this._getPrice(selector);
  }

  async getPrice() {
    let selector = '.bf-product__wrapper .skuBestPrice';
    const specialPrice = await this._getPrice(selector);

    if(specialPrice) {
      return specialPrice;
    }

    selector = '.bf-product__wrapper .skuPrice';
    return this._getPrice(selector);
  }

  async getHasStock() {
    const selector = '.bf-product__wrapper .skuBestInstallmentValue';
    return this.document && this.document(selector).length !== 0;
  }

  async getDescription() {
    const selector = '.bf-product__wrapper .productName';
    const description = this.document(selector).html();
    return description.replace(/(\t|\n)/ig, '').trim();
  }

  async productExists() {
    const selector = '.bf-product__wrapper';
    return this.document && this.document(selector).length !== 0;
  }
}

module.exports = MorenaRosa;
