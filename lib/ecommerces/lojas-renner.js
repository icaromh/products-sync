const Ecommerce = require('../ecommerce')

/**
 * LojasRenner Data Crawler
 * Method: requests (curl like)
 */
class LojasRenner extends Ecommerce {

  constructor(url) {
    super(url)
  }

  async startup(){
    await this.getDocument();
  }

  async _getPrice(selector) {
    const price = this.document(selector)
    return this._normalizeValue(price.text())
  }

  async getOldPrice() {
    const selector = '.main_product_info .old_price';
    return this._getPrice(selector);
  }

  async getPrice() {
    let selector = '.main_product_info .best_price';
    const specialPrice = await this._getPrice(selector);

    if(specialPrice) {
      return specialPrice;
    }

    selector = '.main_product_info .best_price';
    return this._getPrice(selector);
  }

  async getHasStock() {
    const selector = '.main_product_info .best_price';
    return this.document && this.document(selector).length !== 0;
  }

  async getDescription() {
    const selector = '.main_product_info .product_name span';
    const description = this.document(selector).html();
    return description.replace(/(\t|\n)/ig, '').trim();
  }

  async productExists() {
    const selector = '.main_product_info';
    return this.document && this.document(selector).length !== 0;
  }
}

module.exports = LojasRenner;
