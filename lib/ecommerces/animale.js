const Ecommerce = require('../ecommerce')

/**
 * Animale Data Crawler
 * Method: requests (curl like)
 */
class Animale extends Ecommerce {

  constructor(url) {
    super(url)
  }

  async startup(){
    await this.getDocument();
  }

  async _getPrice(selector) {
    const price = this.document(selector)
    return this._normalizeValue(price.text())
  }

  async getOldPrice() {
    const selector = '.product__infos-container .skuListPrice';
    return this._getPrice(selector);
  }

  async getPrice() {
    let selector = '.product__infos-container .skuBestPrice';
    const specialPrice = await this._getPrice(selector);

    if(specialPrice) {
      return specialPrice;
    }

    selector = '.product__infos-container .skuPrice';
    return this._getPrice(selector);
  }

  async getHasStock() {
    const selector = '.product__infos-container .skuPrice';
    return this.document && this.document(selector).length !== 0;
  }

  async getDescription() {
    const selector = '.product__infos-container .productName';
    const description = this.document(selector).html();
    return description.replace(/(\t|\n)/ig, '').trim();
  }

  async productExists() {
    const selector = '.product__infos-container';
    return this.document && this.document(selector).length !== 0;
  }
}

module.exports = Animale;
