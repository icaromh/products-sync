const request = require('request');
const cheerio = require('cheerio');
const puppeteer = require('puppeteer');
const Log = require('./logger');

class Ecommerce {

  /**
   * Attributes available:
   * url,
   * document,
   * browser,
   * page,
   */
  constructor(url) {
    Log.info(`[${this.constructor.name}] Starting with ${url}`)
    this.url = url;
    this.document = null;
    this.browser = null;
    this.page = null;
  }

  /**
   * Close the browser instance avoiding memory leak
   */
  async closeBrowser() {
    if (this.browser) {
      await this.browser.close();
    }
  }

  shouldAbortUrl(url) {
    const URLS_TO_ABORT = [
      'https://connect.facebook.net/en_US/fbevents.js',
      'https://amaro.com/webapp/scripts/advertisement.js',
      'https://tags.srv.stackadapt.com/events.js',
      'https://static.ads-twitter.com/uwt.js',
      'https://www.google-analytics.com/analytics.js',
      'https://secure.adnxs.com/seg?add=15105079&t=1',
    ];

    const WORKS_TO_ABORT = [
      'analytics',
      'events',
      'adnxs.com',
      'ads-twitter'
    ]

    const regexRule = new RegExp(WORKS_TO_ABORT.join('|'), 'ig')
    const shouldBlockByString = regexRule.test(url)

    const urlInBlockList = URLS_TO_ABORT.includes(url);

    return urlInBlockList || shouldBlockByString
  }

  /**
   * Create a new browser and page instance, and nativate to the url especified in the constructor
   * this.page attribute it's used to navigate into the page itself and extract data
   * this.browser attribute it's used to open and close the 'browser'
   */
  async launchBrowser() {
    this.browser = await puppeteer.launch({
      args: ['--no-sandbox', '--disable-setuid-sandbox'],
      ignoreHTTPSErrors: true,
      waitUntil: 'networkidle0'
    });
    this.page = await this.browser.newPage();

    await this.page.setRequestInterception(true);
    this.page.on('request', interceptedRequest => {
      const url = interceptedRequest.url();
      if (url.endsWith('.png') || url.endsWith('.jpg') || this.shouldAbortUrl(url))
        interceptedRequest.abort();
      else
        interceptedRequest.continue();
    });

    await this.page.goto(this.url);
  }

  /**
   * Load the product's HTML as cheerio structure
   * and associate into this.document
   */
  async getDocument() {
    try {
      const html = await this.getHTMLFromUrl(this.url);
      this.document = cheerio.load(html)
    } catch (error) {
      this.document = false
      Log.error(error);
    }
  }

  /**
   * Get the HTML from product's URL
   * @param {String} url prodcuts url
   */
  getHTMLFromUrl(url) {
    return new Promise((resolve, reject) => {
      request(url, (err, res, body) => {
        Log.info(`[${res.statusCode}]: ${url}`);
        if (err) {
          return reject(err);
        }
        else if (res.statusCode !== 200) {
          return reject(new Error('Product Not Found'));
        }
        return resolve(body);
      })
    });
  }

  /**
   * Replace $ for nothing and normalize to english double format
   * @param {String} str Product's value as string
   */
  _normalizeValue(str) {
    if (!str) return 0;

    const value = str
      .replace('R$', '')
      .replace('.', '')
      .replace(',', '.');

    return parseFloat(value, 10);
  }

  async startup() {
    // not implemented
  }

  async teardown() {
    // not implemented
  }

  /**
   * RUN THE SYNC!
   * 1. verify if the product still existing
   * 2. get actual price, old price, if has stock and description
   */
  async run() {
    const exists = await this.productExists();
    if (!exists) return { exists }

    const [price, oldPrice, hasStock, description] = await Promise.all([
      this.getPrice(),
      this.getOldPrice(),
      this.getHasStock(),
      this.getDescription(),
    ])

    return {
      exists,
      hasStock,
      oldPrice,
      price,
      description,
    }
  }
}

module.exports = Ecommerce
