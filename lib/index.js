const ECOMMERCES = require('./ecommerces');

const getEcommerceObj = name => {
  const sanitizedName = name.toLocaleLowerCase();
  return ECOMMERCES[name];
}

async function productsync(data) {
  const { ecommerce, url } = data;

  if(!ecommerce) {
    throw Error(`Missing required parameter: ecommerce`);
  }

  if(!url) {
    throw Error(`Missing required parameter: url`);
  }

  const ecommerceObj = getEcommerceObj(ecommerce);

  if(!ecommerceObj) {
    throw Error(`Ecommerce not implemented: ${ecommerce}`);
  }

  const Ecommerce = new ECOMMERCES[ecommerce](url);
  await Ecommerce.startup();
  const product = await Ecommerce.run();
  await Ecommerce.teardown();

  return product;
}

module.exports = productsync
