const Log = () => {
  const prefix = `${new Date()} [products-sync]`;

  const error = (...args) =>  process.env.DEBUG_SYNCER && console.log(prefix, '[error]', ...args);
  const info = (...args) =>  process.env.DEBUG_SYNCER && console.log(prefix, '[info]', ...args);

  return {
    error,
    info,
  }

}

module.exports = Log()
