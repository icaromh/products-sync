const assert = require('assert');
const Ecommerce = require('./ecommerce');
const Log = require('./logger')

describe('Ecommerce', () => {
  let ecommerce;

  beforeEach(() => {
    ecommerce = new Ecommerce('Example');
  })

  describe('#_normalizeValue', () => {
    var tests = [
      {args: 'R$ 100,00', expected: 100},
      {args: 'R$ 192,28', expected: 192.28},
      {args: 'R$010', expected: 10},
      {args: null, expected: 0},
      {args: '', expected: 0},
      {args: '             R$ 100,00         \n', expected: 100},
    ];

    tests.forEach(test => {
      it('correctly normalize ' + test.args + ' args', () => {
        const res = ecommerce._normalizeValue(test.args);
        assert.equal(res, test.expected);
      });
    });
  });
});
