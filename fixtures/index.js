const fs = require('fs');

const loadFixture = (path) => {
    return new Promise((resolve, reject) => {
        fs.readFile(path, (err, data) => {
            if(err) return reject(err);
            return resolve(data.toString());
        })
    })
}
