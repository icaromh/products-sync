const productsync = require('./lib');
const Log = require('./lib/logger');

//ECOMMERCE EM TESTE
const ecommerce = 'style-market';

//ECOMMERCES DISPONÍVEIS
const ECOMMERCES = {
  'a-brand-br'    : 'a-brand-br',
  'agua-doce'     : 'agua-doce',
  amaro           : 'amaro',
  animale         : 'animale',
  'bo-bo'         : 'bo-bo',
  cantao          : 'cantao',
  'cia-maritima'  : 'cia-maritima',
  'farm-rio'      : 'farm-rio',  //pendente price
  'flaminga'      : 'flaminga',
  fyistore        : 'fyistore',
  'le-lis-blanc'  : 'le-lis-blanc',
  'lezalez'       : 'lezalez', 
  'lojas-renner'  : 'lojas-renner',
  'luiza-barcelos': 'luiza-barcelos',  //error price
  'morena-rosa'   : 'morena-rosa',
  'oh-boy'        : 'oh-boy',
  'rosa-cha'      : 'rosa-cha',
  sacada          : 'sacada',
  shop2gether     : 'shop2gether',
  'style-market'  : 'style-market',
  zinzane         : 'zinzane'
}

const { URLS }  = require('./fixtures/' + ECOMMERCES[ecommerce]);

(async function run() {
  for (url of URLS) {
    try {
      const product = await productsync({
        ecommerce: ECOMMERCES[ecommerce],
        url,
      });
      Log.info(product);
    } catch (error) {
      Log.error(error);
    }
  }
})()
